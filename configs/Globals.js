/****************************
 SECURITY TOKEN HANDLING
 ****************************/
const config = require('./configs');
const Authentication = require('../app/modules/Authentication/Schema').Authtokens;
const Users = require('../app/modules/User/Schema').Users;
const Admin = require('../app/modules/Admin/Schema').Admin;
const RolesSchema = require('../app/modules/Roles/Schema').RolesSchema;
const Model = require('../app/modules/Base/Model');
const _ = require('lodash');
const Moment = require('moment');
const scrypt = require("scrypt");
const i18n = require("i18n");

class Globals {

    // Generate Token
    getToken(params) {
        return new Promise(async (resolve, reject) => {
            try {
                let scryptParameters = scrypt.paramsSync(0.1);
                let kdfResult = scrypt.kdfSync(config.securityToken, scryptParameters);
                let token = kdfResult.toString("hex");
                params.token = kdfResult;
                params.userId = params.id;
                params.tokenExpiryTime = Moment().add(config.tokenExpirationTime, 'minutes');
                delete params.id;
                await Authentication.findOneAndUpdate({ userId: params.userId }, params, { upsert: true, new: true });
                return resolve(token);
            } catch (err) {
                console.log("Get token", err);
                return reject({ message: err, status: 0 });
            }
        });
    }
    // Generate Token
    getTokenWithRefreshToken(params) {
        return new Promise(async (resolve, reject) => {
            try {
                let scryptParameters = scrypt.paramsSync(0.1);
                let kdfResult = scrypt.kdfSync(config.securityToken, scryptParameters);
                let token = kdfResult.toString("hex");
                let kdfRefreshToken = scrypt.kdfSync(config.securityRefreshToken, scryptParameters);
                let refreshToken = kdfRefreshToken.toString("hex");
                params.token = kdfResult;
                params.refreshToken = kdfRefreshToken;
                params.userId = params.id;
                params.tokenExpiryTime = Moment().add(config.tokenExpirationTime, 'minutes');
                delete params.id;
                await Authentication.findOneAndUpdate({ userId: params.userId }, params, { upsert: true, new: true });
                return resolve({ token, refreshToken });
            } catch (err) {
                console.log("Get token", err);
                return reject({ message: err, status: 0 });
            }
        });
    }
    AdminToken(params) {
        return new Promise(async (resolve, reject) => {
            try {
                // Generate Token
                let scryptParameters = scrypt.paramsSync(0.1);
                let kdfResult = scrypt.kdfSync(config.securityToken, scryptParameters);
                let token = kdfResult.toString("hex");

                params.token = kdfResult;
                params.adminId = params.id;
                params.tokenExpiryTime = Moment().add(config.tokenExpirationTime, 'minutes');
                console.log(params);
                const d = await Authentication.findOne({ ipAddress: params.ipAddress, adminId: params.adminId });
                if (_.isEmpty(d)) {
                    await new Model(Authentication).store(params);
                } else {
                    await Authentication.findByIdAndUpdate(d._id, params, { new: true });
                }
                return resolve(token);
            } catch (err) {
                console.log("Get token", err);
                return reject({ message: err, status: 0 });
            }

        });
    }
    generateToken(id) {
        return new Promise(async (resolve, reject) => {
            try {
                let scryptParameters = scrypt.paramsSync(0.1);
                let kdfResult = scrypt.kdfSync(config.securityToken + id, scryptParameters);
                let token = kdfResult.toString("hex");
                return resolve(token);
            } catch (err) {
                console.log("Get token", err);
                return reject({ message: err, status: 0 });
            }

        });
    }
    // Validating Token
    static async isAuthorised(req, res, next) {
        try {
            const token = req.headers.authorization;
            if (!token) return res.status(401).json({ status: 0, message: i18n.__("TOKEN_WITH_API") });

            const authenticate = new Globals();

            const tokenCheck = await authenticate.checkTokenInDB(token);
            if (!tokenCheck) return res.status(401).json({ status: 0, message: i18n.__("INVALID_TOKEN.") });

            const tokenExpire = await authenticate.checkExpiration(token);
            if (!tokenExpire) return res.status(401).json({ status: 0, message: i18n.__("TOKEN_EXPIRED") });

            const userExist = await authenticate.checkUserInDB(token);
            if (!userExist) return res.status(401).json({ status: 0, message: i18n.__("USER_NOT_EXIST_OR_DELETED") });

            if (req.originalUrl) {
                let pathParams = req.originalUrl.split("/");
                if (!pathParams || (pathParams && Array.isArray(pathParams) && _.last(pathParams) && !_.isEqual(_.last(pathParams), 'changePassword'))) {
                    if (config.forceToUpdatePassword) {
                        let shouldPasswordNeedToUpdate = await authenticate.checkPasswordExpiryTime({ userObj: userExist });
                        if (shouldPasswordNeedToUpdate) {
                            return res.json({ status: 0, message: i18n.__("FORCE_PASSWORD_CHANGE") });
                        }
                    }
                }
            }

            if (userExist._id) {
                req.currentUser = userExist;
                if (config.extendTokenTime) {
                    await authenticate.extendTokenTime(userExist._id);
                }
            }

            next();
        } catch (err) {
            console.log("Token authentication", err);
            return res.send({ status: 0, message: err });
        }
    }
    static async isValid(req, res, next) {
        try {
            if (!config.useRefreshToken) {
                return res.status(401).json({ status: 0, message: 'Not authorized to refresh token.' });
            }
            next();
        } catch (err) {
            console.log("isValid", err);
            return res.send({ status: 0, message: err });
        }
    }
    async extendTokenTime(userId) {
        return new Promise(async (resolve, reject) => {
            try {
                const authenticate = await Authentication.findOne({ userId: userId });
                if (authenticate && authenticate.tokenExpiryTime) {
                    let expiryDate = Moment(authenticate.tokenExpiryTime).subtract(2, 'minutes')
                    let now = Moment();
                    if (now > expiryDate) {
                        await Authentication.findOneAndUpdate({ userId: userId }, { tokenExpiryTime: Moment(authenticate.tokenExpiryTime).add(config.tokenExpirationTime, 'minutes') });
                    }
                }
                return resolve();
            } catch (error) {
                reject(error);
            }
        });
    }
    async checkPasswordExpiryTime(data) {
        return new Promise(async (resolve, reject) => {
            try {
                if (data && data.userObj && data.userObj.passwordUpdatedAt) {
                    let lastChangedDate = Moment(data.userObj.passwordUpdatedAt, 'YYYY-MM-DD HH:mm:ss');
                    let currentDate = Moment(new Date(), 'YYYY-MM-DD HH:mm:ss');
                    let duration = Moment.duration(currentDate.diff(lastChangedDate));
                    let months = duration.asMonths();
                    console.log('months', months);
                    if (months >= parseInt(config.updatePasswordPeriod)) {
                        return resolve(true);
                    }
                    return resolve(false);
                }
                return resolve()
            } catch (error) {
                return reject(error);
            }
        });
    }
    static isAdminAuthorised(resource) {
        return async (req, res, next) => {
            console.log('resource', resource);
            try {
                const token = req.headers.authorization;
                if (!token) return res.status(401).json({ status: 0, message: i18n.__("TOKEN_WITH_API") });

                const authenticate = new Globals();

                const tokenCheck = await authenticate.checkTokenInDB(token);
                if (!tokenCheck) return res.status(401).json({ status: 0, message: i18n.__("INVALID_TOKEN.") });

                const tokenExpire = await authenticate.checkExpiration(token);
                if (!tokenExpire) return res.status(401).json({ status: 0, message: i18n.__("TOKEN_EXPIRED") });

                const userExist = await authenticate.checkAdminInDB(token);
                if (!userExist) return res.status(401).json({ status: 0, message: i18n.__("ADMIN_NOT_EXIST") });

                if (userExist._id) {
                    req.currentUser = userExist;
                    // Check admin is authorized to access API
                    if (resource && resource.length && userExist.role && userExist.role._id) {
                        let role = await RolesSchema.findOne({ _id: userExist.role._id }).populate('permissions', { permissionKey: 1 });
                        if (role && role.permissions) {
                            let permissions = _.map(role.permissions, 'permissionKey');
                            console.log('permissions exist', _.difference(resource, permissions).length);
                            if (_.difference(resource, permissions).length != 0) {
                                return res.status(401).json({ status: 0, message: i18n.__("UNAUTHORIZED_TO_ACCESS") });
                            }
                        } else {
                            return res.status(401).json({ status: 0, message: i18n.__("UNAUTHORIZED_TO_ACCESS") });
                        }
                    }
                }
                next();
            } catch (err) {
                console.log("Token authentication", err);
                return res.send({ status: 0, message: err });
            }
        }

    }
    // Check User Existence in DB
    checkUserInDB(token) {
        return new Promise(async (resolve, reject) => {
            try {
                const authenticate = await Authentication.findOne({ token: Buffer.from(token, "hex") });
                if (authenticate && authenticate.userId) {
                    const user = await Users.findOne({ _id: authenticate.userId, isDeleted: false });
                    if (user) return resolve(user);
                }
                return resolve(false);
            } catch (err) {
                console.log("Check user in db")
                return reject({ message: err, status: 0 });
            }

        })
    }
    //Check admin in db
    checkAdminInDB(token) {
        return new Promise(async (resolve, reject) => {
            try {
                const authenticate = await Authentication.findOne({ token: Buffer.from(token, "hex") });
                if (authenticate && authenticate.adminId) {
                    const user = await Admin.findOne({ _id: authenticate.adminId, isDeleted: false });
                    if (user) return resolve(user);
                }
                return resolve(false);
            } catch (err) {
                console.log("Check ADMIN in db")
                return reject({ message: err, status: 0 });
            }
        })
    }
    // Check token in DB
    checkTokenInDB(token) {
        return new Promise(async (resolve, reject) => {
            try {
                let isVerified = scrypt.verifyKdfSync(Buffer.from(token, "hex"), Buffer.from(config.securityToken)); // returns true
                if (!isVerified) { return resolve(false); }
                let filter = { token: Buffer.from(token, "hex") };
                const authenticate = await Authentication.findOne(filter);
                if (authenticate) return resolve(true);
                return resolve(false);
            } catch (err) {
                console.log("Check token in db")
                return resolve({ message: err, status: 0 });
            }
        })
    }
    // Check Token Expiration
    checkExpiration(token) {
        return new Promise(async (resolve, reject) => {
            let status = false;
            const authenticate = await Authentication.findOne({ token: Buffer.from(token, "hex") });
            if (authenticate && authenticate.tokenExpiryTime) {
                let expiryDate = Moment(authenticate.tokenExpiryTime, 'YYYY-MM-DD HH:mm:ss')
                let now = Moment(new Date(), 'YYYY-MM-DD HH:mm:ss');
                if (expiryDate > now) { status = true; resolve(status); }
            }
            resolve(status);
        })
    }
    refreshAccessToken(refreshtoken) {
        return new Promise(async (resolve, reject) => {
            let isVerified = scrypt.verifyKdfSync(Buffer.from(refreshtoken, "hex"), Buffer.from(config.securityRefreshToken)); // returns true
            if (!isVerified) {
                return resolve({ status: 0, message: "Invalid refresh token." });
            }
            let filter = { refreshToken: Buffer.from(refreshtoken, "hex") };
            const authenticationData = await Authentication.findOne(filter);
            if (authenticationData && authenticationData.tokenExpiryTime) {
                let expiryDate = Moment(authenticationData.tokenExpiryTime, 'YYYY-MM-DD HH:mm:ss')
                let now = Moment(new Date(), 'YYYY-MM-DD HH:mm:ss');
                if (expiryDate > now) {
                    return resolve({ status: 0, message: "Access token is not expired yet." });
                } else {
                    const authenticate = new Globals();
                    const { token, refreshToken } = await authenticate.getTokenWithRefreshToken({ id: authenticationData.userId });
                    return resolve({ status: 1, message: "Token refreshed.", access_token: token, refreshToken: refreshToken });
                }
            }
            else {
                return resolve({ status: 0, message: "Wrong refresh token." });
            }
        });
    }
    static decodeAdminForgotToken(token) {
        return new Promise(async (resolve, reject) => {
            const admin = await Admin.findOne({ forgotToken: token });
            if (admin && admin.forgotTokenCreationTime && config.forgotTokenExpireTime) {
                let expiryDate = Moment(admin.forgotTokenCreationTime).add(config.forgotTokenExpireTime);
                let now = Moment();
                if (expiryDate < now) {
                    return resolve(true);
                }
            }
            return resolve(false);
        });
    }
    static decodeUserForgotToken(token) {
        return new Promise(async (resolve, reject) => {
            const user = await Users.findOne({ forgotToken: token });
            if (user && user.forgotTokenCreationTime && config.forgotTokenExpireTime) {
                let expiryDate = Moment(user.forgotTokenCreationTime).add(config.forgotTokenExpireTime);
                let now = Moment();
                if (expiryDate < now) {
                    return resolve(true);
                }
            }
            return resolve(false);
        });
    }
    static decodeUserVerificationToken(token) {
        return new Promise(async (resolve, reject) => {
            const user = await Users.findOne({ verificationToken: token });
            if (user && user.verificationTokenCreationTime && config.verificationTokenExpireTime) {
                let expiryDate = Moment(user.verificationTokenCreationTime).add(config.verificationTokenExpireTime);
                let now = Moment();
                if (expiryDate < now) {
                    return resolve(true);
                }
            }
            return resolve(false);
        });
    }
}

module.exports = Globals;
